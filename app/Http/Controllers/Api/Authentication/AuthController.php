<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request) {
        if($request->username != 'diyar' || $request->password != 'P@ssw0rD') {
            return $this->ResponeError('username or password not valid');
        }

        $jwt = jwtEncodeData(['id' => '1', 'username' => 'Diyar', 'name' => 'Ahmad Mishbakhud Diyar', 'status' => '1', 'address' => 'Kabupaten Rembang, Provinsi Jawa Tengah']);

        return $this->ResponeLoginSuccess($jwt['token'], $jwt['exp'], $jwt['refresh_token']);
    }

    public function profile(Request $request) {
        $jwt = $request->header('Authorization');
        $auth = jwtDecodeData($jwt);

        return $auth;
    }

    public function refreshToken(Request $request) {
        $jwt = $request->header('Authorization');
        $auth = refreshToken($jwt);

        return $auth;
    }

    public function tes() {
        return $this->ref('makanan', 'kanan');
    }

    public function ref($word1, $word2) {
        $array1 = [];
        $array2 = [];

        for($i = 0; $i < strlen($word1) + 1; $i++) {
            for($j = $i + 3; $j < strlen($word1) + 1; $j++) {
                if(strlen($word1) > 2) {
                    array_push($array1, substr($word1, $i, $j));
                    array_push($array2, substr($word2, $i, $j));
                }
            }
        }

        $results = array_unique(array_intersect($array1, $array2));

        $return = [];
        foreach($results as $result) {
            array_push($return, $result);
        }

        return $return;
    }
}
