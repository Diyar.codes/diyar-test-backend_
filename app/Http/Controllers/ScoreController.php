<?php

namespace App\Http\Controllers;

use App\Models\Score;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ScoreController extends Controller
{
    public function index(Request $request) {
        $score = Score::all();

        if ($request->ajax()) {
            return datatables()->of($score)
            ->addColumn('subject', function ($data) {
                return $data->subject->subject;
            })
            ->addColumn('student', function ($data) {
                return $data->student->name;
            })
            ->addColumn('action', function ($data) {
                $button = '<div style="display: flex;">';
                $button .= '<a class="btn btn-info btn-sm mr-2" href="'. route('score.edit',$data->id) .'"><i class="fa fa-edit"></i><span>Edit</span></a>';
                $button .= '
                <form action="'.route('score.delete',$data->id).'" method="POST">
                '.csrf_field().'
                '.method_field("DELETE").'
                <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>';
                $button .= '</div>';

                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
        }


        return view('score.index');
    }

    public function create() {
        $subjects = Subject::all();
        $students = Student::all();

        return view('score.create', compact('subjects', 'students'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'student'  => 'required',
            'subject'  => 'required',
            'score'    => 'required|numeric'
        ]);

        try{
            DB::beginTransaction();

            Score::create([
                'student_id'  => $request->student,
                'subject_id'  => $request->subject,
                'score'       => $request->score
            ]);

            DB::commit();

            return redirect()->route('score.index')->with('success', 'Score Success Added');
        } catch (Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    public function edit($id) {
        $score = Score::where('id', $id)->first();
        $subjects = Subject::all();
        $students = Student::all();

        return view('score.edit', compact('score', 'subjects', 'students'));
    }

    public function update($id, Request $request) {
        $this->validate($request, [
            'score'    => 'required|numeric|between:0,100'
        ]);

        $score = Score::where('id', $id)->first();

        if(!$score) {
            return redirect()->route('score.index');
        }

        try{
            DB::beginTransaction();

            $score->update([
                'student_id' => $request->student,
                'subject_id' => $request->subject,
                'score'   => $request->score
            ]);

            DB::commit();

            return redirect()->route('score.index')->with('success', 'Score Success Updated');
        } catch (Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    public function delete($id) {
        $score = Score::where('id', $id)->first();

        if(!$score) {
            return redirect()->route('score.index');
        }

        try{
            DB::beginTransaction();

            $score->delete();

            DB::commit();

            return redirect()->route('score.index')->with('success', 'Score Success Deleted');
        } catch (Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }
}
