<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ResponeLoginSuccess($token, $exp, $refresh) {
        return response()->json([
            'result'       => "OK",
            'access_token' => $token,
            'token_type'   => "bearer",
            "expire_in"    => $exp,
            "refresh_ttl"  => $refresh
        ]);
    }

    public function ResponeError($message) {
        return response()->json([
            'result'  => "FAILED",
            'message' => $message
        ], 400);
    }

    public function ResponeSuccess($message, $data) {
        return response()->json([
            'status'  => true,
            'message' => $message,
            'data'    => $data
        ]);
    }
}
