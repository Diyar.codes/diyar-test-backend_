<?php
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

function jwtEncodeData($data) {
    $key = env('JWT_SECRET');

    date_default_timezone_set('Asia/Kolkata');

    $token = [
        "iat"      => time(),
        "exp"      => time() + 7200, // 2 hours expire
        "data"     => $data
    ];

    $jwt = JWT::encode($token, $key, 'HS256');
    return ['token' => $jwt, 'exp' => time() + 3600, 'refresh_token' => time() + 3600];
}

function jwtDecodeData($jwt_token) {
    $test = explode(' ', $jwt_token);
    $key = env('JWT_SECRET');

    if ($test[0] != 'Bearer' | count($test) != 2) {
        $response = [
            'result' => "FAILED",
            'message' => "token provided is not valid"
        ];

        return response()->json($response, 400);
    }

    try {
        $decoded = JWT::decode($test[1], new Key($key, 'HS256'));

        return $decoded;

        return response()->json([
            "result" => "OK",
            "username" => $decoded->data->username,
            "name" => $decoded->data->name,
            "status" => $decoded->data->status,
            "address" => $decoded->data->address
        ]);
    } catch(\Firebase\JWT\SignatureInvalidException $e) { // token wrong
        return response()->json([
            'result'  => "FAILED",
            'message' => "token provided is not valid"
        ], 400);
    }catch(\Firebase\JWT\BeforeValidException $e) { // token wrong
        return response()->json([
            'result'  => "FAILED",
            'message' => "token provided is not valid"
        ], 400);
    }catch(\Firebase\JWT\ExpiredException $e) { // token expiration
        return response()->json([
            'result'  => "FAILED",
            'message' => $e->getMessage()
        ], 400);
    }catch(Exception $e) {
        return response()->json([
            'result'  => "FAILED",
            'message' => $e->getMessage()
        ], 400);
    }
}

function refreshToken($jwt_token) {
    $test = explode(' ', $jwt_token);
    $key = env('JWT_SECRET');

    if ($test[0] != 'Bearer' | count($test) != 2) {
        $response = [
            'result' => "FAILED",
            'message' => "token provided is not valid"
        ];

        return response()->json($response, 400);
    }

    try{
        $decoded = JWT::decode($test[1], new Key($key, 'HS256'));
        $decoded->iat = time();
        $decoded->exp = time() + 7200;

        return JWT::encode($decoded, $key, 'HS256');
    }catch ( \Firebase\JWT\ExpiredException $e ) {
        $decoded = JWT::decode($test[1], new Key($key, 'HS256'));
        $decoded->iat = time();
        $decoded->exp = time() + 7200;

        return JWT::encode($decoded, $key, 'HS256');
    }catch ( \Exception $e ){
        return response()->json([
            'result'  => "FAILED",
            'message' => $e->getMessage()
        ], 400);
    }
}
