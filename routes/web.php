<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ScoreController;

Route::get('/', function () { return view('welcome');});

Route::prefix('score')->group(function () {
    Route::get('/', [ScoreController::class,'index'])->name('score.index');
    Route::get('/create', [ScoreController::class,'create'])->name('score.create');
    Route::post('/', [ScoreController::class,'store'])->name('score.store');
    Route::get('/edit/{id}', [ScoreController::class,'edit'])->name('score.edit');
    Route::put('/{id}', [ScoreController::class,'update'])->name('score.update');
    Route::delete('/{id}', [ScoreController::class,'delete'])->name('score.delete');
});
