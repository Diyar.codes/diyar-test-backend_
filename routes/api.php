<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Authentication\AuthController;

Route::namespace('api')->prefix('authentication')->group(function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::get('/refresh-token', [AuthController::class, 'refreshToken']);
    Route::get('/profile', [AuthController::class, 'profile']);

    Route::get('/tes', [AuthController::class, 'tes']);
});
