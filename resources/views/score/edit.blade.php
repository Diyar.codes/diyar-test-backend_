@extends('templates.main')

@section('content')
<form action="{{ route('score.update', $score->id) }}" method="POST" class="mt-3">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="student">Student</label>
        <select class="form-control" id="student" name="student">
            <option value="{{ $score->student->id }}">{{ $score->student->name }}</option>
            @foreach ($students as $student)
                <option {{ old('student') == $student->id ? "selected" : "" }} value="{{ $student->id }}">{{ $student->name }}</option>
            @endforeach
        </select>
        @error('student')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="subject">Subject</label>
        <select class="form-control" id="subject" name="subject">
            <option value="{{ $score->subject->id }}">{{ $score->subject->subject }}</option>
            @foreach ($subjects as $subject)
                <option {{ old('subject') == $subject->id ? "selected" : "" }} value="{{ $subject->id }}">{{ $subject->subject }}</option>
            @endforeach
        </select>
        @error('subject')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="score">Score</label>
        <input type="number" class="form-control" id="score" name="score" placeholder="Input Score" value="{{ old('score', $score->score) }}"">
        @error('score')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <button type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Save</button>
</form>
@endsection
