@extends('templates.main')

@section('content')
    <h3 class="mt-3 mb-1">Score List</h3>

    <a href="{{ route('score.create') }}"><button type="button" class="btn btn-success my-3"><i class="fas fa-plus"></i> Add score</button></a>

    <table class="table table-striped" id="score-index">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Subject</th>
                <th scope="col">Student</th>
                <th scope="col">Score</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#score-index').DataTable({
            "serverSide": true,
            "processing": true,
            "responsive": true,
            "order": [
                [0, 'desc']
            ],
            "ajax": {
                url: "{{ route('score.index') }}",
                type: "GET"
            },
            "columns": [
                {
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    'data': 'subject',
                    'name': 'subject'
                },
                {
                    'data': 'student',
                    'name': 'student'
                },
                {
                    'data': 'score',
                    'name': 'score'
                },
                {
                    'data': 'action',
                    'name': 'action'
                },
            ],
        })
        })
    </script>
@endsection
